from pathlib2 import Path

from makefile_gen import Rule, to_makefile
import makefile_gen.commands as cmd

commit = '72a5fb8'
srcroot = Path('/home/ikolpako/scr/hic21/hicpro-pipeline/runs/production/')
dstroot = Path('/home/ikolpako/scr/hic21/hicpro-runs/') / commit

fcommit = lambda paths: filter(lambda p: commit in str(p), paths)
makerel = lambda paths: map(lambda p: p.relative_to(srcroot), paths)
glob = lambda pattern: makerel(fcommit(srcroot.glob(pattern)))

sample = lambda p: p.parent.stem

srcfiles = dict(
    bam = glob('**/hic/**/*bwt2pairs_interaction.bam'),
    pairs = glob('**/hic/**/*validPairs'),
    pairstat = glob('**/hic/bt2/**/*pairstat'),
    rsstat = glob('**/hic/**/*RSstat'),
)

action = dict(
    bam = lambda *x: cmd.CopyFile('$<', '$@'),
    pairs = lambda *x: cmd.Redirect(cmd.PigzPack(['$<'], 'stdout'), stdout='$@'),
    pairstat = lambda *x: cmd.CopyFile('$<', '$@'),
    rsstat = lambda *x: cmd.CopyFile('$<', '$@'),
)

names = dict(
    bam = 'interaction.bam',
    pairs = 'validPairs.gz',
    pairstat = 'pairstat',
    rsstat = 'rsstat',
)

def mkrule(relpath, group):
    s = str(sample(relpath))
    dstdir = Path('${DST}') / s
    dst = dstdir / names[group]
    src = Path('${SRC}') / relpath
    acts = [
        cmd.MkDir(dstdir, parents=True),
        action[group](dst, src)]
    return Rule(dst, [src], acts, meta=group)

rules = []

for group, relpaths in srcfiles.items():
    rules += [mkrule(rp, group) for rp in relpaths]

vars_ = [
    'SRC={:s}'.format(srcroot),
    'DST={:s}'.format(dstroot)]

groups = []
for group in srcfiles.keys():
    targets = [r.target for r in rules if r.meta==group]
    groups.append(Rule(group, targets, [], phony=True))

all_ = [
    Rule('all', srcfiles.keys(), [], phony=True)
]

rules = vars_ + all_ + groups + rules

print(to_makefile(rules))
