# hic21-hicpro-save

Saving HiC-Pro [run results](https://bitbucket.org/aickley/hic21-samples/)
in the archive
(part of [hic21](https://bitbucket.org/aickley/hic21-meta/) pipeline). 

## Usage

First change the input path/commit settings in the
[Makefile generator](src/code/genmakefile.py) and then
`make` the output
```
   python -m code.genmakefile > Makefile
   make
```
Since the Vital-IT archive is only accesible via the front-end node
there is no point in running the copying command (`make`) via LSF.

## Dependencies

   *  [makefile_gen](https://bitbucket.org/aickley/makefile_gen):
      library for creating Makefile generators.
